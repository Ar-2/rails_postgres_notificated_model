require 'serialized_active_record'

module PostgresNotificatedModel
  module ActsAsNotificated
    extend ActiveSupport::Concern

    included do
    end
    module ClassMethods
      def acts_as_notificated(options = {})
        cattr_accessor :notificated_channel
        self.notificated_channel = options[:channel].to_s || 'record_change'

        after_create  :notify_create 
        after_update  :notify_update
        after_destroy :notify_destroy

        include PostgresNotificatedModel::ActsAsNotificated::LocalInstanceMethods
        def notify(data, channel = self.channel)
          # ActiveRecord::Base.connection.execute "NOTIFY #{channel}, '#{data}'"
          ActiveRecord::Base.connection_pool.with_connection do |connection|
            conn = connection.raw_connection
            conn.async_exec "NOTIFY #{channel}, '#{conn.escape_string(data)}'"
          end
        end
        def subscribe
          ActiveRecord::Base.connection_pool.with_connection do |connection|
            conn = connection.raw_connection
            begin
              conn.async_exec "LISTEN #{channel}"
              loop do 
                conn.wait_for_notify do |channel, pid, result|
                  name, action, id, model = result.split('|')
                  yield name, action, id, model
                end
              end
            ensure
              conn.async_exec "UNLISTEN #{channel}"
            end
          end
        end
        protected def channel
          self.notificated_channel
        end
      end
    end

    module LocalInstanceMethods
      def notify(action = 'update')
        self.class.notify [self.class.name, action, id, to_serialized_json].join('|')
        # ActiveRecord::Base.connection.execute "NOTIFY #{channel}, '#{[self.class.name, action, id, to_serialized_json()].join('|')}'"
      end
      def notify_create
        notify('create')
      end
      def notify_update
        notify('update')
      end
      def notify_destroy
        notify('destroy')
      end

      protected def channel
        self.class.channel
      end
    end
  end
end
ActiveRecord::Base.send :include, PostgresNotificatedModel::ActsAsNotificated