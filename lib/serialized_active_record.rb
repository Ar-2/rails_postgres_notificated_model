module PostgresNotificatedModel
  module SerializedActiveRecord
    extend ActiveSupport::Concern
    included do
      def to_serialized_hash(options = {})
        resource = ActiveModel::SerializableResource.new(self)
        return resource.serializer? ? resource.serializable_hash(options) : self.serializable_hash
      end
      def to_serialized_json(options = {})
        return to_serialized_hash(options).to_json
      end
    end
  end
end
# include the extension 
ActiveRecord::Base.send(:include, PostgresNotificatedModel::SerializedActiveRecord)