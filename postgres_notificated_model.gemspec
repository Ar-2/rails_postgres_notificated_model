$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "postgres_notificated_model/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "postgres_notificated_model"
  s.version     = PostgresNotificatedModel::VERSION
  s.authors     = ["Ar-2"]
  s.email       = ["Ar-2@yandex.ru"]
  s.homepage    = "http://4a.su"
  s.summary     = "Postgres listen/notify for ActiveRecord"
  s.description = "You can notify and listen record changes"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "pg"
  s.add_dependency "active_model_serializers"
  s.add_dependency "rails"

  # s.add_development_dependency "sqlite3"
end
